package excepciones;

public class ErrorNumeroDeLegajo extends Exception{
	
	private static final long serialVersionUID = 1L;

	public ErrorNumeroDeLegajo () {
		
		super ("Valor de n�mero de legajo incorrecto");
	}

}
