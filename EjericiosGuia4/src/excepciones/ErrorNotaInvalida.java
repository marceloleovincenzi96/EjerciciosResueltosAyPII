package excepciones;

public class ErrorNotaInvalida extends Exception{

	private static final long serialVersionUID = 1L;

	public ErrorNotaInvalida () {
		
		super ("Valor de nota de ex�men incorrecto");
	}
}
