package excepciones;

public class DividePorCeroException extends Exception {
	/*
	 * Si al crear la excepcion les tira un warning el compilador, solo hagan click sobre el icono de la advertencia y elijan
	 * la opci�n "Add default serial version ID". 
	 */

	private static final long serialVersionUID = 1L;
	
	//Esta es la forma est�ndar de crear una excepcion. Si quisieramos que ya tenga un mensaje predefinido, simplemente no pasamos ning�n parametro.
	public DividePorCeroException (String s) {
		
		super (s);
	}

}
