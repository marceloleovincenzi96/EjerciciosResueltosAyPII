package tests;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import principal.Ejercicio8;

public class PruebaEjercicio8 {
	
	public static void main (String [] args) throws IOException {
		
		Ejercicio8 ej8 = new Ejercicio8();
	
		BufferedReader lector = new BufferedReader (new InputStreamReader (System.in));
		
		System.out.println("Ingrese la ruta del archivo: ");
		String rutaArchivo = lector.readLine();
		
		System.out.println("Ingrese la palabra a buscar en el archivo: ");
		String palabra = lector.readLine();
		
		System.out.println(palabra + "aparece: " + ej8.encontrarYContarPalabraEnArchivo(palabra, rutaArchivo) + "veces en el archivo.");
		
		
	}

}
