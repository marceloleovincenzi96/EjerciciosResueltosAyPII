package tests;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import principal.Ejercicio9;

public class PruebaEjercicio9 {
	
	public static void main (String [] args) throws IOException {
		
		Ejercicio9 ej9 = new Ejercicio9();
		BufferedReader lector = new BufferedReader (new InputStreamReader (System.in));
		
		System.out.println("Ingrese nombre de archivo a crear: ");
		String nombreArchivo = lector.readLine();
		
		System.out.println("Ingrese el texto a escribir: ");
		String texto = lector.readLine();
		
		ej9.escribirPalabraEnArchivo(texto, nombreArchivo);
	}
	
	

}
