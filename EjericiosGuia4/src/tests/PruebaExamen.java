package tests;
import principal.ExamenEj5;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import excepciones.ErrorNotaInvalida;
import excepciones.ErrorNumeroDeLegajo;

public class PruebaExamen {

	public static void main(String[] args) throws IOException {
		
		BufferedReader lector = new BufferedReader (new InputStreamReader (System.in));
		
		System.out.println("Ingrese el nombre del alumno");
		String nombre = lector.readLine();
		
		System.out.println("Ingrese la fecha del examen");
		String fecha = lector.readLine();
		
		System.out.println("Ingrese el n�mero de legajo");
		int legajo = Integer.parseInt(lector.readLine());
		
		ExamenEj5 examen = null;
		
		try {
			examen = new ExamenEj5 (fecha, nombre, legajo);
			
		} catch (ErrorNumeroDeLegajo e) {
			
			System.out.println(e.getMessage());
			return;
		}
		
		System.out.println("Ingrese la nota del examen");
		int nota = Integer.parseInt(lector.readLine());
		
		try {
			
			examen.setNota(nota);
			
		}
		
		catch (ErrorNotaInvalida e) {
			
			System.out.println(e.getMessage());
			return;
		}
	}



}

