package principal;

import excepciones.ErrorNotaInvalida;
import excepciones.ErrorNumeroDeLegajo;

/*Escriba una clase Examen que tenga las propiedades fecha, nombreAlumno,
numeroDeLegajo, nota y los m�todos setFecha, setNombreAlumno, setNumeroDeLegajo y
setNota que asignaran valores a cada una de las propiedades de la clase. Un valor de nota
v�lido ser� un n�mero entero del 1 al 10 y un n�mero de legajo v�lido ser� un n�mero
entero entre 10000 y 15000. Los m�todos deben lanzar las excepciones del punto 5
ErrorNotaInvalida y ErrorNumeroDeLegajo cuando corresponda. El constructor de la
clase Examen debe establecer la fecha, el nombre del alumno y el n�mero de legajo con
valores que recibir� a trav�s de par�metros y deber� lanzar las excepciones que
correspondan. Cree una clase de prueba llamado cree un objeto de tipo Examen con
valores previamente ingresados por teclado (solicitados al usuario), y que luego modifique
la propiedad nota de dicho objeto con un valor tambi�n ingresado por teclado. Capture
todas las excepciones que puedan ocurrir en el m�todo test e imprima sus mensajes.*/

public class ExamenEj5 {
	
	private String fecha;
	private String nombreAlumno;
	private int numeroDeLegajo;
	private int nota;
	
	public ExamenEj5 (String fecha, String nombreAlumno, int numeroDeLegajo) throws ErrorNumeroDeLegajo {
		
		setFecha (fecha);
		setNombreAlumno (nombreAlumno);
		setNumeroDeLegajo (numeroDeLegajo);
		
	}

	public void setFecha(String fecha) {
		
		this.fecha = fecha;
	}
	

	public void setNombreAlumno(String nombreAlumno) {
		
		this.nombreAlumno = nombreAlumno;
	}
	

	public void setNumeroDeLegajo(int numeroDeLegajo) throws ErrorNumeroDeLegajo {
		
		if (numeroDeLegajo < 10000 || numeroDeLegajo > 15000) {
			
			throw new ErrorNumeroDeLegajo ();
		}
		
		this.numeroDeLegajo = numeroDeLegajo;
	}

	public void setNota(int nota) throws ErrorNotaInvalida {
		
		if (nota < 1 || nota > 10) {
			
			throw new ErrorNotaInvalida();
		}
		
		this.nota = nota;
	}
	
	

}
