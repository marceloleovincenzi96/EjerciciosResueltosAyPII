package principal;

import java.io.FileWriter;
import java.io.IOException;

public class Ejercicio9 {
	
	//Implemente un m�todo que lea una l�nea de caracteres ingresada por teclado y la escriba en un archivo.
	
	/*
	 * Importante: el archivo se guardar� en la carpeta del proyecto en el workspace
	 */

	
	public void escribirPalabraEnArchivo (String linea, String nombreDeArchivo) throws IOException {
		
		FileWriter f0 = new FileWriter (nombreDeArchivo);
		
		f0.write (linea);
		
		f0.close();
	}	

}
