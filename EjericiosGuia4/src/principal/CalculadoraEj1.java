package principal;

/*
 * Crear una clase Calculadora y agregar un m�todo �dividir� el mismo debe tratar la
 * excepci�n est�ndar de Java que se produce al realizar una divisi�n por cero.
 */

public class CalculadoraEj1 {
	
	public double sumar (double a, double b) {
		
		return a+b;
	}
	
	public double restar (double a, double b) {
		
		return a-b;
	}
	
	public double multiplicar (double a, double b) {
		
		return a*b;
	}
	
	public double dividir (double a, double b) throws ArithmeticException{
		
		if (b == 0) {
			
			throw new ArithmeticException ("El divisor no puede ser 0");
		}
		
		return a/b;
	}
	
	

}
