package principal;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/*
 *  Implemente un m�todo que lea una l�nea de caracteres ingresada por teclado e imprima en pantalla la cantidad de palabras que contiene.
 */

public class Ejercicio7 {
	
	public static void main (String [] args) throws IOException {
		
		BufferedReader lector = new BufferedReader (new InputStreamReader (System.in));
		
		System.out.println("Por favor, ingrese una oraci�n o texto: ");
		String texto = lector.readLine();
		
		StringTokenizer st = new StringTokenizer (texto);
		
		System.out.println("Cantidad de palabras: " + st.countTokens());
		
	}
	
	

}
