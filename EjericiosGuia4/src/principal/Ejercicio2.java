package principal;
import excepciones.DividePorCeroException;

/*
 * Modificar el ejercicio anterior para que la excepción lanzada sea una nueva excepción llamada DividePorCeroException​ creada por el programador.
 */

public class Ejercicio2 {
	
	public double sumar (double a, double b) {
		
		return a+b;
	}
	
	public double restar (double a, double b) {
		
		return a-b;
	}
	
	public double multiplicar (double a, double b) {
		
		return a*b;
	}
	
	public double dividir (double a, double b) throws DividePorCeroException{
		
		if (b == 0) {
			
			throw new DividePorCeroException ("El divisor no puede ser 0");
		}
		
		return a/b;
	}
	
	

}
