package principal;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;

public class Ejercicio8 {
	
	BufferedReader lector;
	
	//Implemente un m�todo que reciba una palabra y un archivo; y cuente cuantas veces aparece esa palabra en el archivo.
	
	public int encontrarYContarPalabraEnArchivo (String palabra, String rutaDelArchivo) throws IOException, FileNotFoundException {
		
		FileReader file = new FileReader (rutaDelArchivo);
		lector = new BufferedReader (file);
		String linea;
		StringTokenizer st;
		int ocurrencias = 0;
		
		while ((linea = lector.readLine()) != null) {
			
			st = new StringTokenizer (linea);
			
			while (st.hasMoreTokens()) {
				
				if (st.nextToken().equalsIgnoreCase(palabra)) { // con equalsIgnoreCase ignoro las may�sculas
					
					ocurrencias++;
				}
				
			}
			
		}
		
		return ocurrencias;
	}
	
	
	


}
