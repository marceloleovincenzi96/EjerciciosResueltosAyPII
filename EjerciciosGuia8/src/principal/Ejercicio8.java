package principal;

//Escriba un m�todo recursivo que tome dos n�meros enteros y calcule la multiplicaci�n entre ellos, usando s�lo sumas.

public class Ejercicio8 {
	
	public static void main (String [] args) {
		
		int a = 6;
		
		int b = 3;
		
		System.out.println(multiplicarSumando (a,b));
	}
	
	public static int multiplicarSumando (int a, int b) {
		
		int resultado = 0;
		
		if (a == 0 || b == 0) {
			
			return 0;
		}
		
		if (a == 1) {
			
			return b;
		}
		
		if (b == 1) {
			
			return a;
		}
		
		resultado = a + multiplicarSumando (a, b-1);
		
		return resultado;
	}

}
