package principal;

public class Ejercicio9 {
	
	
	public static void main (String [] args) {
		
		int a = 200;
		
		int b = 10;
		
		int [] final1 = dividirRestando (a,b);
		
		for (int i = 0; i < 2; i++) {
			
			System.out.println(final1[i]);
		}
		
	}
	
	public static int [] dividirRestando (int dividendo, int divisor) {
		
		
		if (divisor == 0) {
			
			throw new ArithmeticException ("El divisor no puede ser 0");
			
		} 
			else if (dividendo == 0) {
			
			return new int [] {0,0};
				
		}
		
		if (dividendo==divisor) {
			
			return new int [ ] {1,0};
		}
		
		if (dividendo < divisor) {
			
			return new int [] {0, dividendo};
		}
		
		int [] preRes = dividirRestando (dividendo - divisor, divisor);
		
		preRes [0]++;
		
		return preRes;
		
	}

}
