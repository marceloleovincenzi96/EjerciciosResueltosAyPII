package principal;

/*
 * Escriba un m�todo recursivo que tome un entero n y devuelva la suma de los primeros n n�meros
 */

public class Ejercicio2 {
	
	public static int sumarNPrimerosNumeros (int n) {
		
		if (n == 0) {
			
			return 0;
		}
		
		return n + sumarNPrimerosNumeros (n-1);
	}
	
	
	
	public static void main (String [] args) {
		
		int numero = 9;
		
		System.out.println(sumarNPrimerosNumeros(numero));
		
		
	}

}
