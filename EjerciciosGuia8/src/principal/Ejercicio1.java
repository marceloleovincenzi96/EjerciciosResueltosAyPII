package principal;

/*
 * Escribir un m�todo recursivo que tome una palabra y diga si es un pal�ndromo
 * (Palabra o expresi�n que es igual si se lee de izquierda a derecha que de derecha a izquierda).
 */


public class Ejercicio1 {
	
	public static boolean esPalindromo (String palabra) {
		
		String posiblePalindromo = palabra.toLowerCase();
		
		
		if (posiblePalindromo.length()==1 || posiblePalindromo.length()==0) {
			
			return true;
		}
		
		if (posiblePalindromo.charAt(0) == posiblePalindromo.charAt(posiblePalindromo.length()-1)) {
			
			return esPalindromo(posiblePalindromo.substring(1, posiblePalindromo.length()-1));
		}
		
		
		return false;
		
		
	}
	

	public static void main (String [] args) {
		
		String palabra = "Neuquen";
		
		System.out.println(esPalindromo(palabra));
	}
	
	

}
