package principal;

public class Ejercicio2 {

	public static double potencia(double x, int n) {
		
		double result = 1.0; // una asignación es una operación elemental. por lo tanto, vale 1.
		
		for (int i = 0; i < n; i++) { 
			
		/* dentro del for tenemos una asignacion (vale 1), una comparación (vale 1) y un incremento (vale 1).
		* Sin embargo, cabe recordar que este segmento del codigo se repetira n veces. Por lo tanto, vale n.
		*/
			
			result *= x; //multiplicaciones valen 1 por ser operaciones elementales
		}
			
		return result; //retornar un valor vale 1
	}
	
	//T = 1 + n*(3 + 1) + 1 = 2 + 3n + 3 = 3n + 5 => O(n)

}
