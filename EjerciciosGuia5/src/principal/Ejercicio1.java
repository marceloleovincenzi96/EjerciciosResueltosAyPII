package principal;

public class Ejercicio1 {

	public static int maximo(int x, int y) {

		if (x >= y) { //evaluamos la condicion del if, la cual es una operación elemental (comparación de valores simples), o sea, vale 1.
			
			return x; //retornar un valor simple vale 1.
			
		} else {
			
			return y; //retornar un valor simple vale 1.
		}
	}
	
	/*
	 * Tiempo de ejecución del if: T = T(C) + max {T(S1), T(S2)}
	 * Por lo tanto, tenemos que: T = 1 + max {1,1}
	 * T = 1 + 1 = 2 = O (2) = O (c) (c por ser constante)
	 */

}
