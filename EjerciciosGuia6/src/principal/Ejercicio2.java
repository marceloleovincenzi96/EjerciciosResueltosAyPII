package principal;

import java.util.Stack;

/*
 * Escribir un programa que verifique si una palabra es palíndromo 
 * (es decir que una cadena es igual a su inversa. P.ej. las cadenas “1456541” y “145541” son palíndromos).
 */

public class Ejercicio2 {

	public boolean esPalindromo(String secuenciaDeCaracteres) {

		boolean esPalindromo = false;

		Stack<Character> stack = new Stack<Character>(); 

		for (int i = 0; i < secuenciaDeCaracteres.length(); i++) {

			Character caracter = secuenciaDeCaracteres.charAt(i);

			stack.push(caracter);

		}

		String ordenInverso = "";

		while (!stack.isEmpty()) {

			ordenInverso += stack.pop();
		}

		if (ordenInverso.equalsIgnoreCase(secuenciaDeCaracteres)) {
			
			esPalindromo = true;
		}
		
		return esPalindromo;

	}

}
