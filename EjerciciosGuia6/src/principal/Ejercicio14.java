package principal;

import java.util.*;

/*
 * Escriba un m�todo que devuelva true si una lista de enteros es sublista de otra. Por
	ejemplo, si tenemos las Listas L1=[22,14,6] y L2=[39, 41, 17, 22, 14, 6, 3, 11, 73, 81]
	entonces el m�todo devolver� true porque L1 es una sublista de L2. Pero si tenemos otra
	lista L3=[39, 41, 22, 17, 14, 3, 11, 73, 6, 81], vemos que L1 no es sublista de L3 por lo que
	el m�todo llamado con L1 y L3 debe devolver false.

 */

//Creditos para el alumno Lucas G�mez de la comisi�n de los viernes por resolverlo. Esta es su soluci�n.

public class Ejercicio14 {
	
	public static void main(String[] args) {

		List<Integer> subLista = Arrays.asList(22, 14, 6);
		List<Integer> lista = Arrays.asList(39, 41, 22, 17, 14, 3, 11, 73, 6,
				81);

		System.out.println(verificarSublista(lista, subLista));

		// list1.add(22);
		// list1.add(14);
		// list1.add(6);

	}

	private static boolean verificarSublista(List<Integer> principal, List<Integer> subLista) {

		ListIterator<Integer> l1 = subLista.listIterator();

		ListIterator<Integer> l2 = principal.listIterator();

		int num = l1.next();

		while (l2.hasNext()) {

			if (l2.next().equals(num)) {

				while (l1.hasNext()) {

					if (!l2.next().equals(l1.next())) {

						return false;
					}
				}
				return true;
			}
		}
		return false;
	}
}
