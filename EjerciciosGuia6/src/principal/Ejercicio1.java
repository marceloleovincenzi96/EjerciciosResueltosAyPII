package principal;

import java.util.Stack;


//Escribir un programa que lee una secuencia de caracteres y los imprime en orden inverso.

public class Ejercicio1 {
	
	public void imprimirCaracteresOrdenInverso (String secuenciaDeCaracteres) {
		
		Stack <Character> stack = new Stack <Character>(); //las colecciones basadas en tipos primitivos (como char) son ineficientes. hay que usar Characters
		
		for (int i = 0; i < secuenciaDeCaracteres.length(); i++) {
			
			Character caracter = secuenciaDeCaracteres.charAt(i);
			
			stack.push(caracter);
			
		}
		
		while (!stack.isEmpty()) {
			
			System.out.println(stack.pop());
		}
		
		
		
	}
	
	

}
