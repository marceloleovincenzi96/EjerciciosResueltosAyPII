package test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import principal.Ejercicio2;


public class TestsEjercicio2 {
	
	Ejercicio2 ej2;
	
	@Before
	public void before () {
		
		ej2 = new Ejercicio2 ();
	}
	
	@Test
	public void casoPositivo1 () {
		
		Assert.assertTrue(ej2.esPalindromo("Neuquen"));
	}
	
	@Test
	public void casoPositivo2 () {
		
		Assert.assertTrue(ej2.esPalindromo("1456541"));
	}
	
	@Test
	public void casoNegativo () {
		
		Assert.assertFalse(ej2.esPalindromo("Diego"));
	}
	
	

}
